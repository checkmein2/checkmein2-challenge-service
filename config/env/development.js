/* eslint-disable import/no-absolute-path*/
const secrets = require('/run/secrets/app_secrets');
/* eslint-enable import/no-absolute-path*/

module.exports = {
    session: 'secret-boilerplate-token',
    database: `mongodb://${process.env.MONGO_URL}:${process.env.MONGO_PORT}/Challenges`,
    secretKey: secrets.SECRET_KEY || 'secret_key'
};
