const router = require('express').Router();
const winston = require('winston');
const repository = require('../repository/challenges')();
const checkinRepository = require('../repository/checkin')();
const status = require('http-status');
const logger = require('../config/logger');

router.use((req, res, next) => {
    winston.log('info', `METHOD: ${req.method} URL: ${req.url}`);
    next();
});

const authenticate = (req, res, next) => {
    if (!req.user) {
        logger.error(`401 METHOD: ${req.method} URL: ${req.url}`);
        res.status(status.UNAUTHORIZED).json({ error: 'UNAUTHORIZED REQUEST' });
    } else {
        next();
    }
};

//
// GET APIs
//

// get all checkins
router.get('/challenges/checkins', (req, res, next) => {
    checkinRepository
        .getCheckins()
        .then(checkins => {
            res.status(status.OK).json(checkins);
        })
        .catch(next);
});
// get checkins for user_id
router.get('/challenges/checkins/user/:user_id', (req, res, next) => {
    checkinRepository
        .getCheckinsForUser(req.params.user_id)
        .then(checkins => {
            res.status(status.OK).json(checkins);
        })
        .catch(next);
});
// get checkins for challenge
router.get('/challenges/checkins/challenge/:challenge_id', (req, res, next) => {
    checkinRepository
        .getCheckinsForChallenge(req.params.challenge_id)
        .then(checkins => {
            res.status(status.OK).json(checkins);
        })
        .catch(next);
});
// add a checkin
router.post('/challenges/checkin', authenticate, (req, res, next) => {
    checkinRepository
        .createCheckin(req.body.checkin, req.user.id)
        .then(checkin => {
            res.status(status.OK).json(checkin);
        })
        .catch(next);
});
// delete a checkin
router.delete(
    '/challenges/checkin/:checkin_id',
    authenticate,
    (req, res, next) => {
        checkinRepository
            .deleteCheckin(req.params.checkin_id, req.user.id)
            .then(response => {
                res.status(status.OK).json(response);
            })
            .catch(next);
    }
);


// get all challenges
router.get('/challenges', (req, res, next) => {
    repository
        .getChallenges()
        .then(challenges => {
            res.status(status.OK).json(challenges);
        })
        .catch(next);
});
// get a challenge providing challenge id
router.get('/challenges/challenge/:challenge_id', (req, res, next) => {
    repository
        .getChallenge(req.params.challenge_id)
        .then(challenge => {
            res.status(status.OK).json(challenge);
        })
        .catch(next);
});
// get all challenges for a campaign id
router.get('/challenges/:campaign_id', (req, res, next) => {
    repository
        .getChallengesForCampaign(req.params.campaign_id)
        .then(challenges => {
            res.status(status.OK).json(challenges);
        })
        .catch(next);
});
// get all challenges for a sponsor
router.get('/challengesForSponsor', authenticate, (req, res, next) => {
    repository
        .getChallengesForSponsor(req.user.id)
        .then(challenges => {
            res.status(status.OK).json(challenges);
        })
        .catch(next);
});
// get all challenges for a participant
router.get('/challengesForParticipant', authenticate, (req, res, next) => {
    repository
        .getChallengesForParticipant(req.user.id)
        .then(challenges => {
            res.status(status.OK).json(challenges);
        })
        .catch(next);
});

//
// POST APIs
//

// add a new challenge
router.post('/challenges', authenticate, (req, res, next) => {
    repository
        .createChallenge(req.body.challenge, req.user.id)
        .then(challenge => {
            res.status(status.OK).json(challenge);
        })
        .catch(next);
});
// add participant to challenge
router.post(
    '/challenges/:challenge_id/participant',
    authenticate,
    (req, res, next) => {
        repository
            .addParticipantForChallenge(req.params.challenge_id, req.user.id)
            .then(challenge => {
                res.status(status.OK).json(challenge);
            })
            .catch(next);
    }
);
// add sponsor to challenge
router.post(
    '/challenges/:challenge_id/sponsor',
    authenticate,
    (req, res, next) => {
        const sponsor = Object.assign(req.body.sponsor, {
            user_id: req.user.id
        });
        repository
            .addSponsorForChallenge(req.params.challenge_id, sponsor)
            .then(challenge => {
                res.status(status.OK).json(challenge);
            })
            .catch(next);
    }
);

//
// PUT APIs
//

// // edit a challenge providing challenge id
router.put('/challenges/challenge/:challenge_id', 
    authenticate,
    (req, res, next) => {
    repository
        .updateChallenge(req.params.challenge_id, req.body.challenge, req.user.id)
        .then(challenge => {
            res.status(status.OK).json(challenge);
        })
        .catch(next);
});
// update a sponsor providing challenge id and sponsor id
router.put(
    '/challenges/:challenge_id/sponsor',
    authenticate,
    (req, res, next) => {
        repository
            .updateSponsor(
                req.params.challenge_id,
                req.body.sponsor,
                req.user.id
            )
            .then(challenge => {
                res.status(status.OK).json(challenge);
            })
            .catch(next);
    }
);

//
// DELETE APIs
//

// delete a challenge
router.delete(
    '/challenges/challenge/:challenge_id',
    authenticate,
    (req, res, next) => {
        repository
            .deleteChallenge(req.params.challenge_id, req.user.id)
            .then(response => {
                res.status(status.OK).json(response);
            })
            .catch(next);
    }
);
// delete a sponsor from a challenge
router.delete(
    '/challenges/:challenge_id/sponsor',
    authenticate,
    (req, res, next) => {
        repository
            .deleteSponsorFromChallenge(req.params.challenge_id, req.user.id)
            .then(challenge => {
                res.status(status.OK).json(challenge);
            })
            .catch(next);
    }
);
// delete a participant from a challenge
router.delete(
    '/challenges/:challenge_id/participant',
    authenticate,
    (req, res, next) => {
        repository
            .deleteParticipantFromChallenge(
                req.params.challenge_id,
                req.user.id
            )
            .then(challenge => {
                res.status(status.OK).json(challenge);
            })
            .catch(next);
    }
);
module.exports = router;
