const fs = require('fs');
const status = require('http-status');

const version = JSON.parse(fs.readFileSync('package.json')).version;

const health = (req, res) => {
    res.status(200).send(version);
};

module.exports = (app) => {
    app.get('/health', health);
};
