let Ajv = require('ajv');
let ajv = Ajv({ allErrors: true, removeAdditional: 'all' });

let userSchema = require('./create-challenge.json');
ajv.addSchema(userSchema, 'create-challenge');

function errorResponse(schemaErrors) {
    let errors = schemaErrors.map(error => {
        return {
            path: error.dataPath,
            message: error.message
        };
    });
    return {
        status: 'failed',
        errors: errors
    };
}

let validateSchema = schemaName => {
    return async (ctx, next) => {
        let valid = await ajv.validate(schemaName, ctx.request.body);
        if (!valid) {
            ctx.body = errorResponse(ajv.errors);
        }
        if (next) {
            next();
        }
    };
};

export default validateSchema;
