const Checkin = require('./models/checkin');
const Challenge = require('./models/challenges');

function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}

function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
    var earthRadiusKm = 6371;

    var dLat = degreesToRadians(lat2-lat1);
    var dLon = degreesToRadians(lon2-lon1);

    lat1 = degreesToRadians(lat1);
    lat2 = degreesToRadians(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    return earthRadiusKm * c;
}

module.exports = () => {

    const getCheckins = () =>
        new Promise((resolve, reject) => {
            Checkin.find((err, checkins) => {
                if (err) {
                    return reject(
                        new Error(
                            'An error occured fetching all checkins, err:' +
                                err
                        )
                    );
                }
                return resolve(checkins);
            });
        });
    const getCheckinsForChallenge = challenge_id =>
        new Promise((resolve, reject) => {
            Checkin.find(
                {
                    challenge_id: challenge_id
                }, (err, checkins) => {
                if (err) {
                    return reject(
                        new Error(
                            `An error occured fetching 
                            checkins for ${challenge_id}`
                        )
                    );
                }
                return resolve(checkins);
            });
        });

    const getCheckinsForUser = user_id =>
        new Promise((resolve, reject) => {
            Checkin.find(
                {
                    user_id: user_id
                }, (err, checkins) => {
                if (err) {
                    return reject(
                        new Error(
                            `An error occured fetching 
                            checkins for ${user_id}`
                        )
                    );
                }
                return resolve(checkins);
            });
        });
    const getCheckin = (challenge_id, user_id) =>
        new Promise((resolve, reject) => {
            Checkin.find(
                {
                    challenge_id: challenge_id,
                    user_id: user_id
                }, (err, checkin) => {
                if (err) {
                    return reject(
                        new Error(
                            `An error occured fetching 
                            checkin for ${challenge_id} ${user_id}`
                        )
                    );
                }
                return resolve(checkin);
            });
        });

    const createCheckin = (data, user_id) =>
        new Promise((resolve, reject) => {
            const { challenge_id, checkin_data: { lat, lng, numeric, photo } } = data;
            Challenge.findById(challenge_id, (err, challenge) => {
                if (err) {
                    return reject(
                        new Error(
                            `An error occured fetching 
                            challenge ${challenge_id}`
                        )
                    );
                }

                const checkLocation = challenge.checkin_types.lat && challenge.checkin_types.lng;
                const checkNumeric = challenge.checkin_types.numeric;
                const checkPhoto = challenge.checkin_types.photo;

                errors = [];

                if (checkPhoto && photo === '') {
                    errors.push('Please upload a photo');    
                }
                if (checkNumeric && numeric !== challenge.checkin_types.numeric) {
                    errors.push('Please enter a correct value');
                }
                if (checkLocation) {
                    const distance = distanceInKmBetweenEarthCoordinates(lat, lng, 
                        challenge.checkin_types.lat, challenge.checkin_types.lng);
                    if (distance > 1) {    
                        errors.push('Please enter a correct location');
                    }    
                }

                data.user_id = user_id;
                data.completed = !errors.length;

                if ( data.completed ) {
                    const checkin = new Checkin(data);
                    return checkin.save(err => {
                        if (err) {
                            return reject(
                                new Error(`An error occured saving a new checkin`)
                            );
                        }
                        return resolve(checkin);
                    });
                } else {
                    return resolve({
                        completed: false,
                        message: errors.join('.\n')
                    });
                }
            });

        });

    const deleteCheckin = (checkin_id, user_id) =>
        new Promise((resolve, reject) => {
            Checkin.findById(checkin_id, (err, checkin) => {
                if (checkin.user_id !== user_id) {
                    return reject(
                        new Error(`An error ocurred deleting a checkin`)
                    );
                }
                checkin.remove(err => {
                    if (err) {
                        return reject(
                            new Error(`An error ocurred deleting a checkin`)
                        );
                    }
                    return resolve({
                        _id: checkin_id,
                        success: true
                    });
                });
            });
        });
    
    return {
        getCheckin,
        getCheckins,
        getCheckinsForChallenge,
        getCheckinsForUser,
        createCheckin,
        deleteCheckin
    };
};
