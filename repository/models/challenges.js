const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Sponsor = new Schema(
    {
        user_id: { type: String, required: true },
        description: { type: String, required: true }
    },
    { _id: false }
);

const Participant = new Schema(
    {
        user_id: { type: String, required: true }
    },
    { _id: false }
);

const CheckInType = new Schema(
    {
        photo: { type: Boolean },
        numeric: { type: Number },
        lat: { type: Number },
        lng: { type: Number }
    },
    { _id: false }
);

const Challenge = new Schema({
    campaign_id: { type: String, required: true },
    name: { type: String, required: true },
    description: { type: String, required: true },
    media_id: { type: String, required: true },
    owner_id: { type: String },
    checkin_types: CheckInType,
    sponsors: [Sponsor],
    participants: [Participant]
});

module.exports = mongoose.model('Challenge', Challenge);
