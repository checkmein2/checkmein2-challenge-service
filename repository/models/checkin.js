const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CheckInData = new Schema(
    {
        photo: { type: String },
        numeric: { type: Number },
        lat: { type: Number },
        lng: { type: Number }
    },
    { _id: false }
);

const Checkin = new Schema({
    challenge_id: { type: String, required: true },
    user_id: { type: String, required: true },
    checkin_data: CheckInData,
    completed: { type: Boolean }
});

module.exports = mongoose.model('Checkin', Checkin);