const Challenge = require('./models/challenges');

module.exports = () => {
    const getChallenges = () =>
        new Promise((resolve, reject) => {
            Challenge.find((err, challenges) => {
                if (err) {
                    return reject(
                        new Error(
                            'An error occured fetching all challenges, err:' +
                                err
                        )
                    );
                }
                return resolve(challenges);
            });
        });
    const getChallenge = challenge_id =>
        new Promise((resolve, reject) => {
            Challenge.findById(challenge_id, (err, challenge) => {
                if (err) {
                    return reject(
                        new Error(
                            `An error occured fetching 
                            challenge ${challenge_id}`
                        )
                    );
                }
                return resolve(challenge);
            });
        });
    const getChallengesForCampaign = campaign_id =>
        new Promise((resolve, reject) => {
            Challenge.find(
                {
                    campaign_id: campaign_id
                },
                (err, challenge) => {
                    if (err) {
                        return reject(
                            new Error(
                                `An error occured fetching 
                                challenges for campaign ${campaign_id}`
                            )
                        );
                    }
                    return resolve(challenge);
                }
            );
        });
    const getChallengesForSponsor = sponsor_id =>
        new Promise((resolve, reject) => {
            Challenge.find(
                {
                    'sponsors.user_id': sponsor_id
                },
                (err, challenges) => {
                    if (err) {
                        return reject(
                            new Error(
                                `An error occured fetching 
                                challenges for sponsor ${sponsor_id}`
                            )
                        );
                    }
                    return resolve(challenges);
                }
            );
        });
    const getChallengesForParticipant = participant_id =>
        new Promise((resolve, reject) => {
            Challenge.find(
                {
                    'participants.user_id': participant_id
                },
                (err, challenges) => {
                    if (err) {
                        return reject(
                            new Error(
                                `An error occured fetching 
                                challenges for participant ${participant_id}`
                            )
                        );
                    }
                    return resolve(challenges);
                }
            );
        });
    const createChallenge = (data, user_id) =>
        new Promise((resolve, reject) => {
            data.owner_id = user_id;
            if (data.sponsor && data.sponsor.description) {
                data.sponsors = [
                    {
                        user_id,
                        description: data.sponsor.description
                    }
                ];
            }
            const challenge = new Challenge(data);
            return challenge.save(err => {
                if (err) {
                    return reject(
                        new Error(`An error occured saving a new challenge`)
                    );
                }
                return resolve(challenge);
            });
        });
    const addParticipantForChallenge = (challenge_id, user_id) =>
        new Promise((resolve, reject) => {
            const challenge = Challenge.findById(
                challenge_id,
                (err, challenge) => {
                    challenge.participants.push({ user_id });
                    return challenge.save(err => {
                        if (err) {
                            return reject(
                                new Error(
                                    `An error occured adding a participant to a challenge`
                                )
                            );
                        }
                        return resolve({
                            _id: challenge_id,
                            user_id
                        });
                    });
                }
            );
        });
    const addSponsorForChallenge = (challenge_id, sponsor) =>
        new Promise((resolve, reject) => {
            const challenge = Challenge.findById(
                challenge_id,
                (err, challenge) => {
                    challenge.sponsors.push(sponsor);
                    return challenge.save(err => {
                        if (err) {
                            return reject(
                                new Error(
                                    `An error occured adding a sponsor to a challenge`
                                )
                            );
                        }
                        return resolve(challenge);
                    });
                }
            );
        });
    const updateChallenge = (challenge_id, new_challenge, user_id) =>
        new Promise((resolve, reject) => {
            new_challenge.sponsors = [
                {
                    user_id,
                    description: new_challenge.sponsor.description
                }
            ];
            Challenge.findByIdAndUpdate(
                challenge_id,
                { $set: new_challenge },
                (err, challenge) => {
                    if (err) {
                        return reject(
                            new Error(`An error occured updating a challenge`)
                        );
                    }
                    return resolve(challenge);
                }
            );
        });
    const updateSponsor = (challenge_id, new_sponsor, sponsor_id) =>
        new Promise((resolve, reject) => {
            Challenge.findById(challenge_id, (err, challenge) => {
                const sponsors = challenge.sponsors.map(
                    sponsor =>
                        sponsor.user_id === sponsor_id
                            ? Object.assign(sponsor, new_sponsor)
                            : sponsor
                );
                Object.assign(challenge, { sponsors });
                return challenge.save((err, challenge) => {
                    if (err) {
                        return reject(
                            new Error(
                                `An error occured updating a sponsor for a challenge`
                            )
                        );
                    }
                    return resolve(challenge);
                });
            });
        });
    const deleteChallenge = (challenge_id, user_id) =>
        new Promise((resolve, reject) => {
            Challenge.findById(challenge_id, (err, challenge) => {
                if (challenge.owner_id !== user_id) {
                    return reject(
                        new Error(`An error occured deleteing a challenge`)
                    );
                }
                challenge.remove(err => {
                    if (err) {
                        return reject(
                            new Error(`An error occured deleteing a challenge`)
                        );
                    }
                    return resolve({
                        _id: challenge_id,
                        success: true
                    });
                });
            });
        });
    const deleteSponsorFromChallenge = (challenge_id, sponsor_id) =>
        new Promise((resolve, reject) => {
            Challenge.findById(challenge_id, (err, challenge) => {
                const sponsors = challenge.sponsors.filter(
                    sponsor => sponsor.user_id !== sponsor_id
                );
                Object.assign(challenge, { sponsors });
                challenge.save(err => {
                    if (err) {
                        return reject(
                            new Error(
                                `An error occured deleteing a sponsor from a challenge`
                            )
                        );
                    }
                    return resolve(challenge);
                });
            });
        });
    const deleteParticipantFromChallenge = (challenge_id, participant_id) =>
        new Promise((resolve, reject) => {
            Challenge.findById(challenge_id, (err, challenge) => {
                const participants = challenge.participants.filter(
                    participant => participant.user_id !== participant_id
                );
                Object.assign(challenge, { participants });
                challenge.save(err => {
                    if (err) {
                        return reject(
                            new Error(
                                `An error occured deleteing a participant from a challenge`
                            )
                        );
                    }
                    return resolve({
                        _id: challenge_id,
                        user_id: participant_id
                    });
                });
            });
        });
    return {
        getChallenges,
        getChallenge,
        getChallengesForCampaign,
        getChallengesForSponsor,
        getChallengesForParticipant,
        createChallenge,
        addSponsorForChallenge,
        addParticipantForChallenge,
        updateChallenge,
        updateSponsor,
        deleteChallenge,
        deleteSponsorFromChallenge,
        deleteParticipantFromChallenge
    };
};
