const call = {
    request: {
        api: '/challenge/:challenge_id/sponsor',
        url: 'http://localhost:5000/challenge/58cbfa15fdddfd392cf8e290/sponsor',
        method: 'post',
        headers: 'Content-Type: application/json',
        body: {
            sponsor: {
                user_id: 'user3',
                description: 'test description'
            }
        }
    },
    response: {
        challenge: {
            _id: '58cbfa15fdddfd392cf8e290',
            campaign_id: '1',
            name: 'test',
            media_id: 'mid',
            __v: 2,
            participants: [
                {
                    user_id: 'user1',
                    checked_in: false,
                    _id: '58cfd480bcc2696307526bc0'
                },
                {
                    user_id: 'user1',
                    checked_in: true,
                    _id: '58cbfa15fdddfd392cf8e291'
                }
            ],
            sponsors: [
                {
                    user_id: 'sponsor1',
                    description: 'sponsor 1 description test',
                    _id: '58cbfa15fdddfd392cf8e294'
                },
                {
                    user_id: 'sponsor1',
                    description: 'sponsor 2 description',
                    _id: '58cbfa15fdddfd392cf8e293'
                },
                {
                    user_id: 'user3',
                    description: 'test description',
                    _id: '58cfd58bb3b377654aadd76b'
                }
            ]
        }
    }
};
