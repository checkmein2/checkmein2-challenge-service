const call = {
    request: {
        api: '/challenge/:challenge_id/participant/:participant_id',
        url: 'http://localhost:5000/challenge/58cbfa15fdddfd392cf8e290/participant/p1',
        method: 'put',
        headers: 'Content-Type: application/json',
        body: {
            participant: {
                checked_in: true
            }
        }
    },
    response: {
        challenge: {
            _id: '58cbfa15fdddfd392cf8e290',
            campaign_id: '1',
            name: 'test',
            media_id: 'mid',
            __v: 0,
            participants: [
                {
                    user_id: 'p1',
                    checked_in: true,
                    _id: '58cbfa15fdddfd392cf8e292'
                },
                {
                    user_id: 'p2',
                    checked_in: true,
                    _id: '58cbfa15fdddfd392cf8e291'
                }
            ],
            sponsors: [
                {
                    user_id: '1',
                    description: 'sponsor 1 description',
                    _id: '58cbfa15fdddfd392cf8e294'
                },
                {
                    user_id: '2',
                    description: 'sponsor 2 description',
                    _id: '58cbfa15fdddfd392cf8e293'
                }
            ]
        }
    }
};
