const call = {
    request: {
        api: '/challenge/:challenge_id',
        url: 'http://localhost:5000/challenge/58cfd6aa66a3b0689ade5369',
        method: 'get',
        headers: '',
        body: {}
    },
    response: {
        challenge: {
            _id: '58cfd6aa66a3b0689ade5369',
            campaign_id: '1',
            name: 'challenge 2',
            media_id: 'mid2',
            __v: 0,
            participants: [
                {
                    user_id: 'user1',
                    checked_in: false,
                    _id: '58cfd6aa66a3b0689ade536b'
                }
            ],
            sponsors: [
                {
                    user_id: 'sponsor1',
                    description: 'sponsor 1 description test',
                    _id: '58cfd6aa66a3b0689ade536a'
                }
            ]
        }
    }
};
