const call = {
    request: {
        api: '/challenge',
        url: 'http://localhost:5000/challenge',
        method: 'post',
        headers: 'Content-Type: application/json',
        body: {
            challenge: {
                campaign_id: '1',
                name: 'challenge 2',
                media_id: 'mid2',
                participants: [
                    {
                        user_id: 'user1',
                        checked_in: false
                    }
                ],
                sponsors: [
                    {
                        user_id: 'sponsor1',
                        description: 'sponsor 1 description test'
                    }
                ]
            }
        }
    },
    response: {
        challenge: {
            __v: 0,
            campaign_id: '1',
            name: 'challenge 2',
            media_id: 'mid2',
            _id: '58cfd6aa66a3b0689ade5369',
            participants: [
                {
                    user_id: 'user1',
                    checked_in: false,
                    _id: '58cfd6aa66a3b0689ade536b'
                }
            ],
            sponsors: [
                {
                    user_id: 'sponsor1',
                    description: 'sponsor 1 description test',
                    _id: '58cfd6aa66a3b0689ade536a'
                }
            ]
        }
    }
};
