const call = {
    request: {
        api: '/challenges',
        url: 'http://localhost:5000/challenges',
        method: 'get',
        headers: 'Content-Type: application/json',
        body: {}
    },
    response: {
        challenges: [
            {
                _id: '58cbfa15fdddfd392cf8e290',
                campaign_id: '1',
                name: 'test',
                media_id: 'mid',
                __v: 5,
                participants: [
                    {
                        user_id: 'user1',
                        checked_in: false,
                        _id: '58cfd480bcc2696307526bc0'
                    },
                    {
                        user_id: 'user1',
                        checked_in: true,
                        _id: '58cbfa15fdddfd392cf8e291'
                    },
                    {
                        user_id: 'user3',
                        checked_in: true,
                        _id: '58cfd606669a31664bcdd565'
                    }
                ],
                sponsors: [
                    {
                        user_id: 'sponsor1',
                        description: 'sponsor 1 description test',
                        _id: '58cbfa15fdddfd392cf8e294'
                    },
                    {
                        user_id: 'sponsor1',
                        description: 'sponsor 2 description',
                        _id: '58cbfa15fdddfd392cf8e293'
                    },
                    {
                        user_id: 'user3',
                        description: 'test description',
                        _id: '58cfd58bb3b377654aadd76b'
                    }
                ]
            },
            {
                _id: '58cfd6aa66a3b0689ade5369',
                campaign_id: '1',
                name: 'challenge 2',
                media_id: 'mid2',
                __v: 0,
                participants: [
                    {
                        user_id: 'user1',
                        checked_in: false,
                        _id: '58cfd6aa66a3b0689ade536b'
                    }
                ],
                sponsors: [
                    {
                        user_id: 'sponsor1',
                        description: 'sponsor 1 description test',
                        _id: '58cfd6aa66a3b0689ade536a'
                    }
                ]
            }
        ]
    }
};
