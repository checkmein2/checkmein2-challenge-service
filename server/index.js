const express = require('express');
const helmet = require('helmet');
const winston = require('winston');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('../config');
const health = require('../api/health');
const logger = require('../config/logger');
const status = require('http-status');

const app = express();
mongoose.connect(config.database);

const router = require('../api');
var jwt = require('express-jwt');

health(app);
app.use(bodyParser.json());
app.use(
    jwt({
        secret: config.secretKey,
        credentialsRequired: false,
        getToken: function fromHeaderOrQuerystring(req) {
            if (req.headers.authorization !== '') {
                return req.headers.authorization;
            }
            return null;
        }
    })
);

if (process.env.NODE_ENV === 'development') {
    const swaggerTools = require('swagger-tools');
    const swaggerDocument = require('../docs/swagger.json');
    swaggerTools.initializeMiddleware(swaggerDocument, middleware => {
        // Serve the Swagger documents and Swagger UI
        app.use(
            middleware.swaggerUi({
                swaggerUi: '/challenges/docs/apis',
                apiDocs: '/challenges/api-docs/apis'
            })
        );
    });
    app.use('/challenges/api-docs/docs', express.static('docs'));
    app.use('/challenges/api-docs', express.static('docs/challenge'));
}

app.use(router);
app.use((err, req, res, next) => {
    logger.error({
        message: err.message,
        error: err
    });
    res.status(err.status || status.INTERNAL_SERVER_ERROR);
    res.json({
        message: err.message,
        error: process.env.NODE_ENV === 'development' ? err : {}
    });
});

app.listen(config.port, () => {
    winston.log('info', `Server started on ${config.port}`);
});

module.exports = app;
